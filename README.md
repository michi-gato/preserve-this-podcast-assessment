## About
This is the "Preserve This Podcast: An Assessment of the 2018-2020 Project" report formatted as a book on R Markdown and **bookdown** (https://github.com/rstudio/bookdown). The report was written by Meridith Beck Mink.

[Preserve This Podcast](http://preservethispodcast.org/) is an Andrew W. Mellon grant-funded project hosted by the Metropolitan New York Library Council (METRO) to help podcasters protect their work against the threats of digital decay. The project began in January 2018 and is funded through January 2020. Alongside this report, the Preserve This Podcast team produced a [zine workbook](http://preservethispodcast.org/#zine) and a [5-part podcast series](http://preservethispodcast.org/#podcast), as well as a series of traveling workshops. Don't forget to listen, subscribe, rate, and review wherever you get your podcasts!

## More about bookdown
For more information about bookdown, please see the page "[Get Started](https://bookdown.org/yihui/bookdown/get-started.html)" at https://bookdown.org/yihui/bookdown/ for how to compile this example into HTML. You may generate a copy of the book in `bookdown::pdf_book` format by calling `bookdown::render_book('index.Rmd', 'bookdown::pdf_book')`. More detailed instructions are available here https://bookdown.org/yihui/bookdown/build-the-book.html

## Contact 
Feel free to email [preservethispodcast at protonmail dot com](mailto:preservethispodcast@protonmail.com) if you have questions or concerns.
